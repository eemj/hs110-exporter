package config

import (
	"github.com/spf13/viper"
	"path/filepath"
	"runtime"
)

const applicationName = "hs110-exporter"

type ClientConfig struct {
	Host     string `mapstructure:"host"`
	Username string `mapstructure:"username,omitempty"`
	Password string `mapstructure:"password,omitempty"`
}

type Config struct {
	Clients []ClientConfig `mapstructure:"clients"`
}

func NewConfig() (c *Config, err error) {
	v := viper.New()

	v.SetConfigName("config")
	v.SetConfigType("yaml")

	switch runtime.GOOS {
	case "windows":
		v.AddConfigPath(filepath.Join("$PROGRAMDATA", applicationName))
		v.AddConfigPath(filepath.Join("$APPDATA", applicationName))
		v.AddConfigPath(filepath.Join("$USERPROFILE", applicationName))
	case "darwin":
		v.AddConfigPath(filepath.Join("/Library", "Application Support", applicationName))
		v.AddConfigPath(filepath.Join("$HOME", "/Library", "Application Support", applicationName))
	case "linux", "freebsd":
		v.AddConfigPath(filepath.Join("$XDC_CONFIG_HOME/", applicationName))
		v.AddConfigPath(filepath.Join("$XDC_CONFIG_DIRS/", applicationName))
		v.AddConfigPath(filepath.Join("/etc/", applicationName))
		v.AddConfigPath(filepath.Join("$HOME/", applicationName))
		v.AddConfigPath(filepath.Join("$HOME/.config", applicationName))
	}

	v.AddConfigPath(".")

	if err = v.ReadInConfig(); err != nil {
		return
	}

	c = new(Config)
	err = v.Unmarshal(c)

	return
}
