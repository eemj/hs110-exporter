package client

import "encoding/json"

type SysInfo struct {
	System struct {
		GetSysInfo struct {
			SoftwareVersion string `json:"sw_ver"`
			HardwareVersion string `json:"hw_ver"`
			Model           string `json:"model"`
			DeviceID        string `json:"deviceId"`
			OEMID           string `json:"oemId"`
			HardwareID      string `json:"hwId"`
			RSSI            int64  `json:"rssi"`
			Latitude        int64  `json:"latitude_i"`
			Longitude       int64  `json:"longitude_i"`
			Alias           string `json:"alias"`
			Status          string `json:"status"`
			MicType         string `json:"mic_type"`
			Feature         string `json:"feature"`
			MAC             string `json:"mac"`
			Updating        int8   `json:"updating"`
			LedOff          int8   `json:"led_off"`
			RelayState      int8   `json:"relay_state"`
			OnTime          int64  `json:"on_time"`
			IconHash        string `json:"icon_hash"`
			DevName         string `json:"dev_name"`
			ActiveMode      string `json:"active_mode"`
			NextAction      struct {
				Type int8 `json:"type"`
			} `json:"next_action"`
			NTCState int8 `json:"ntc_state"`
			ErrCode  int8 `json:"err_code"`
		} `json:"get_sysinfo"`
	} `json:"system"`
}

const sysInfoRequest = `{"system":{"get_sysinfo":null}}`

func GetSysInfo(client ClientInterface) (sysInfo *SysInfo, err error) {
	data, err := client.Request([]byte(sysInfoRequest))

	if err != nil {
		return
	}

	sysInfo = &SysInfo{}

	err = json.Unmarshal(data, sysInfo)

	return
}
