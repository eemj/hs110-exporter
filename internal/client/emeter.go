package client

import "encoding/json"

type Emeter struct {
	Emeter struct {
		GetRealtime struct {
			CurrentMA int64 `json:"current_ma"`
			VoltageMV int64 `json:"voltage_mv"`
			PowerMW   int64 `json:"power_mw"`
			TotalWH   int64 `json:"total_wh"`
			ErrorCode int64 `json:"err_code"`
		} `json:"get_realtime"`
	} `json:"emeter"`
}

const emeterRequest = `{"emeter":{"get_realtime":{}}}`

func GetEmeter(client ClientInterface) (emeter *Emeter, err error) {
	data, err := client.Request([]byte(emeterRequest))

	if err != nil {
		return
	}

	emeter = &Emeter{}

	err = json.Unmarshal(data, emeter)

	return
}
