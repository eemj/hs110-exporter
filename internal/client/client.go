package client

type ClientInterface interface {
	Request([]byte) ([]byte, error)
	Addr() string
}
