package exporter

import (
	"log"

	"gitlab.com/eemj/hs110-exporter/internal/client"

	"github.com/prometheus/client_golang/prometheus"
)

type Exporter struct {
	clients []client.ClientInterface

	online     *prometheus.Desc
	metadata   *prometheus.Desc
	current    *prometheus.Desc
	voltage    *prometheus.Desc
	power      *prometheus.Desc
	totalPower *prometheus.Desc
}

func NewExporter(clients []client.ClientInterface) (exporter *Exporter) {
	labels := prometheus.Labels{}
	labelNames := []string{"addr", "alias", "mac_address"}

	exporter = &Exporter{
		clients: clients,

		online: prometheus.NewDesc(
			"kasa_online",
			"Device online.",
			labelNames,
			labels,
		),
		metadata: prometheus.NewDesc(
			"kasa_metadata",
			"Device metadata.",
			[]string{
				"alias",
				"mac_address",
				"hardware_version",
				"software_version",
				"model",
				"feature",
			},
			labels,
		),
		current: prometheus.NewDesc(
			"kasa_current",
			"Current flowing through device.",
			labelNames,
			labels,
		),
		voltage: prometheus.NewDesc(
			"kasa_voltage",
			"Voltage flowing through device.",
			labelNames,
			labels,
		),
		power: prometheus.NewDesc(
			"kasa_power",
			"Power flowing through device in W.",
			labelNames,
			labels,
		),
		totalPower: prometheus.NewDesc(
			"kasa_total_power",
			"Power consumption since device connected in kWh.",
			labelNames,
			labels,
		),
	}

	return
}

func (k *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- k.online
	ch <- k.metadata
	ch <- k.current
	ch <- k.voltage
	ch <- k.power
	ch <- k.totalPower
}

func (k *Exporter) Collect(ch chan<- prometheus.Metric) {
	for _, c := range k.clients {
		sysinfo, err := client.GetSysInfo(c)

		if err != nil {
			ch <- prometheus.MustNewConstMetric(
				k.online,
				prometheus.GaugeValue,
				0,
			)
			log.Print(err)
			return
		}

		ch <- prometheus.MustNewConstMetric(
			k.metadata,
			prometheus.GaugeValue,
			1,
			sysinfo.System.GetSysInfo.Alias,
			sysinfo.System.GetSysInfo.MAC,
			sysinfo.System.GetSysInfo.HardwareVersion,
			sysinfo.System.GetSysInfo.SoftwareVersion,
			sysinfo.System.GetSysInfo.Model,
			sysinfo.System.GetSysInfo.Feature,
		)

		emeter, err := client.GetEmeter(c)

		if err != nil {
			ch <- prometheus.MustNewConstMetric(
				k.online,
				prometheus.GaugeValue,
				0,
			)
			return
		}

		labels := []string{
			c.Addr(),
			sysinfo.System.GetSysInfo.Alias,
			sysinfo.System.GetSysInfo.MAC,
		}

		ch <- prometheus.MustNewConstMetric(
			k.online,
			prometheus.GaugeValue,
			1,
			labels...,
		)
		ch <- prometheus.MustNewConstMetric(
			k.current,
			prometheus.GaugeValue,
			float64(emeter.Emeter.GetRealtime.CurrentMA),
			labels...,
		)
		ch <- prometheus.MustNewConstMetric(
			k.voltage,
			prometheus.GaugeValue,
			float64(emeter.Emeter.GetRealtime.VoltageMV),
			labels...,
		)
		ch <- prometheus.MustNewConstMetric(
			k.power,
			prometheus.GaugeValue,
			float64(emeter.Emeter.GetRealtime.PowerMW),
			labels...,
		)
		ch <- prometheus.MustNewConstMetric(
			k.totalPower,
			prometheus.CounterValue,
			float64(emeter.Emeter.GetRealtime.TotalWH),
			labels...,
		)
	}
}
