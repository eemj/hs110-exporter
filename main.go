package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/eemj/hs110-exporter/internal/client"
	oclient "gitlab.com/eemj/hs110-exporter/internal/client/v1.0.4"
	nclient "gitlab.com/eemj/hs110-exporter/internal/client/v1.1.0"
	"gitlab.com/eemj/hs110-exporter/internal/client/version"
	"gitlab.com/eemj/hs110-exporter/internal/config"
	"gitlab.com/eemj/hs110-exporter/internal/exporter"
)

type HTTPServer struct {
	mux     *http.ServeMux
	clients []client.ClientInterface
}

func NewHTTPServer(
	clients []client.ClientInterface,
) (server *HTTPServer) {
	server = &HTTPServer{
		mux:     http.NewServeMux(),
		clients: clients,
	}

	server.mux.HandleFunc("/metrics", server.GetMetrics)

	return
}

func (s *HTTPServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.mux.ServeHTTP(w, r)
}

func (s *HTTPServer) GetMetrics(w http.ResponseWriter, r *http.Request) {
	registry := prometheus.NewRegistry()

	exp := exporter.NewExporter(s.clients)

	if err := registry.Register(exp); err != nil {
		json.NewEncoder(w).Encode(map[string]interface{}{
			"code":  http.StatusInternalServerError,
			"error": err.Error(),
		})
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	promhttp.HandlerFor(registry, promhttp.HandlerOpts{}).ServeHTTP(w, r)
}

func main() {
	cfg, err := config.NewConfig()

	if err != nil {
		log.Fatal(err)
	}

	clients := make([]client.ClientInterface, 0)

	for _, clientConfig := range cfg.Clients {
		vr, err := version.DetermineVersion(clientConfig.Host)

		if err != nil {
			log.Fatal(err)
		}

		var client client.ClientInterface

		switch vr {
		case version.Version104:
			client = oclient.NewClient(&oclient.ClientOptions{Host: clientConfig.Host})
		case version.Version110:
			client, err = nclient.NewClient(&nclient.ClientOptions{
				Host:     clientConfig.Host,
				Username: clientConfig.Username,
				Password: clientConfig.Password,
			})
		}

		if err != nil {
			log.Fatal(err)
		}

		clients = append(clients, client)
	}

	srv := NewHTTPServer(clients)

	if err := http.ListenAndServe(":9089", srv); err != nil {
		log.Fatal(err)
	}
}
