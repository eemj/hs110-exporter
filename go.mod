module gitlab.com/eemj/hs110-exporter

go 1.15

require (
	github.com/prometheus/client_golang v1.9.0
	github.com/spf13/viper v1.7.1
)
